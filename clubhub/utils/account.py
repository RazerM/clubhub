from clubhub.models import User, Partner, Child


def is_partner_or_child(user):
    is_partner = Partner.query.filter_by(partner=user).first()
    is_child = Child.query.filter_by(child=user).first()
    return is_partner or is_child


def get_parent(child):
    child_of_user = Child.query.filter_by(child=child).first()
    if child_of_user:
        return User.query.get(child_of_user.user_id)
    return None


def get_other_half(user=None, partner=None):
    if user:
        relation = Partner.query.filter_by(user=user).first()
        if relation:
            return User.query.get(relation.partner_id)
    elif partner:
        relation = Partner.query.filter_by(partner=partner).first()
        if relation:
            return User.query.get(relation.user_id)
    return None


def get_children_of_user(user):
    children = []
    relations = Child.query.filter_by(user=user).all()
    for relation in relations:
        children.append(User.query.get(relation.child_id))
    if children:
        return children
    else:
        return None
