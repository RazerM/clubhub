from clubhub.models import Company, Employment


def get_company_roles(user, company):
    roles = []
    if user == company.officer:
        roles.append('Officer')
    if user == company.deputy:
        roles.append('Deputy')
    return roles


def is_company_admin(user, company=None):
    if company:
        if user in [company.officer, company.deputy]:
            return True
    else:
        companies = Company.query.all()
        for company in companies:
            if is_company_admin(user, company):
                return True
    return False


def is_company_admin_of_user(admin, user):
    companies = []
    for company in Company.query.all():
        if is_company_admin(admin, company):
            companies.append(company)
    for company in companies:
        if is_employee(user, company):
            return True
    return False


def get_companies_to_admin(user):
    companies = []
    for company in Company.query.all():
        if is_company_admin(user, company):
            companies.append(company)
    return companies


def is_employee(user, company=None):
    if company:
        employment = Employment.query.filter_by(
            user=user, company=company).all()
    else:
        employment = Employment.query.filter_by(user=user).all()

    if employment:
        return True
    else:
        return False
