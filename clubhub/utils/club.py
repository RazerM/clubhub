from clubhub.models import Club
from .membership import get_active_memberships,\
    is_current_member, is_requesting_membership


def get_club_roles(user, club):
    roles = []
    if user == club.chairman:
        roles.append('Chairman')
    if user == club.secretary:
        roles.append('Secretary')
    if user == club.treasurer:
        roles.append('Treasurer')
    if user == club.inventory:
        roles.append('Inventory')
    return roles


def get_clubs_to_admin(user):
    clubs = []
    for club in Club.query.all():
        if is_club_admin(user, club):
            clubs.append(club)
    return clubs


def create_club_name_dict():
    # create a dictionary that maps club ids to club names
    club_name_dict = {}
    for club in Club.query.all():
        club_name_dict[club.id] = club.name
    return club_name_dict


def is_club_admin(user, club=None):
    if club:
        if user in [club.chairman, club.secretary,
                    club.treasurer, club.inventory]:
            return True
    else:
        clubs = Club.query.all()
        for club in clubs:
            if is_club_admin(user, club):
                return True
    return False


def get_my_clubs(user):
    my_clubs = []
    for active_membership in get_active_memberships(user):
        my_clubs.append(Club.query.get(active_membership.club_id))
    return my_clubs


def is_club_admin_of_user(admin, user):
    clubs = []
    for club in Club.query.all():
        if is_club_admin(admin, club):
            clubs.append(club)
    for club in clubs:
        if is_current_member(user, club) or\
                is_requesting_membership(user, club):
            return True
    return False
