from functools import wraps

from flask import abort, request, flash
from flask_login import current_user
from wtforms import ValidationError

from clubhub.models import User, SuperAdmin
from .club import is_club_admin, is_club_admin_of_user
from .company import is_company_admin, is_company_admin_of_user


# ------------------------------------------------------------------------------
# Validation
# ------------------------------------------------------------------------------

def validate_user_email(self, field):
    field.data = field.data.lower()
    if not User.query.filter_by(email=field.data).first():
        raise ValidationError('No user with that email found.')
        return False
    else:
        return True


def validate_time_range(self, start, end):
    if start > end:
        raise ValidationError('"Valid Till" must be later than "Valid From".')
        return False
    else:
        return True


# ------------------------------------------------------------------------------
# Argument parsing
# ------------------------------------------------------------------------------

def parse_arg(arg, datatype):
    arg = request.args.get(arg)
    if not arg:
        None
    try:
        arg = datatype(arg)
    except Exception:
        None
    return arg


# ------------------------------------------------------------------------------
# Permissions
# ------------------------------------------------------------------------------

def super_admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated or\
           not is_super_admin(current_user):
            abort(401)
        return f(*args, **kwargs)
    return decorated_function


def is_super_admin(user):
    if SuperAdmin.query.filter_by(user=user).first():
        return True
    return False


def is_admin_of_user(admin, user):
    return is_club_admin_of_user(admin, user) or\
        is_company_admin_of_user(admin, user)


def require_club_admin(admin, club):
    if not is_club_admin(admin, club) and not is_super_admin(admin):
        flash('You must be club admin to see this page.')
        abort(403)


def require_company_admin(admin, company):
    if not is_company_admin(admin, company) and not is_super_admin(admin):
        flash('You must be company admin to see this page.')
        abort(403)


def require_user_admin(admin, user):
    if not is_admin_of_user(admin, user) and not is_super_admin(admin):
        flash('You must be admin to see this page.')
        abort(403)
