import datetime

from clubhub.models import User, Membership


def get_active_memberships(user, club=None):
    if club:
        memberships = Membership.query.filter_by(
            user=user, club=club, confirmed=True).all()
    else:
        memberships = Membership.query.filter_by(
            user=user, confirmed=True).all()
    active_memberships = []
    for membership in memberships:
        if valid_membership(membership):
            active_memberships.append(membership)
    return active_memberships


def get_pending_memberships(user, club=None):
    if club:
        pending_memberships = Membership.query.filter_by(
            user=user, club=club, confirmed=False).all()
    else:
        pending_memberships = Membership.query.filter_by(
            user=user, confirmed=False).all()
    return pending_memberships


def get_active_members_of_club(club):
    date = datetime.date.today()
    memberships = Membership.query.filter_by(club=club, confirmed=True)
    memberships = memberships.filter(
        Membership.valid_from <= date,
        Membership.valid_till >= date
    ).all()
    members = []
    for membership in memberships:
        members.append(membership.user)
    return members, memberships


def get_membership_requests(club):
    users = []
    requests = []
    unconfirmed_memberships = Membership.query.filter_by(
        club=club, confirmed=False).all()
    for unconfirmed_membership in unconfirmed_memberships:
        user = User.query.get(unconfirmed_membership.user_id)
        users.append(user)
        requests.append(unconfirmed_membership)
    return users, requests


def is_current_member(user, club):
    memberships = Membership.query.filter_by(
        user=user, club=club, confirmed=True).all()
    for membership in memberships:
        if valid_membership(membership):
            return True
    return False


def is_requesting_membership(user, club):
    memberships = Membership.query.filter_by(
        user=user, club=club, confirmed=False).all()
    if memberships:
        return True
    return False


def valid_membership(membership, date=datetime.date.today()):
    # check if the membership is valid for the specified date
    if date >= membership.valid_from and date <= membership.valid_till:
        return True
    else:
        return False


def get_conflicting_memberships(
        user_id, club_id, valid_from, valid_till, exclude_membership_id=None):
    existing_memberships = Membership.query.filter_by(
        user_id=user_id, club_id=club_id, confirmed=True).all()
    if not existing_memberships:
        return None
    conflicting_memberships = []
    if exclude_membership_id:
        existing_memberships.remove(
            Membership.query.get(exclude_membership_id))
    for existing_membership in existing_memberships:
        if existing_membership.valid_from <= valid_from and\
                existing_membership.valid_till >= valid_from:
            conflicting_memberships.append(existing_membership)
        elif existing_membership.valid_from <= valid_till and\
                existing_membership.valid_till >= valid_till:
            conflicting_memberships.append(existing_membership)
        elif existing_membership.valid_from >= valid_from and\
                existing_membership.valid_till <= valid_till:
            conflicting_memberships.append(existing_membership)
    return conflicting_memberships


def is_pending_membership(user_id, club_id, valid_from, valid_till):
    return Membership.query.filter_by(
        user_id=user_id, club_id=club_id,
        valid_from=valid_from, valid_till=valid_till,
        confirmed=False).first()
