from .core import db
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flask_login import UserMixin


class Profile(db.Model):
    """A profile is linked to a user and contains additional, optional
    information about the user."""
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), nullable=False, unique=True)
    user = db.relationship('User', uselist=False, backref='profile')
    phone = db.Column(db.String(32))
    about = db.Column(db.Text)

    def __repr__(self):
        return "<Profile ({})>".format(self.user)


class SuperAdmin(db.Model):
    """The superadmin group has full control of the website content."""
    __tablename__ = 'superadmin'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), unique=True)
    user = db.relationship('User', backref='superadmin')


class Club(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    description = db.Column(db.Text)
    email = db.Column(db.String(256))
    website = db.Column(db.String(256))
    iban = db.Column(db.String(64))
    chairman_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    chairman = db.relationship('User', foreign_keys=chairman_id)
    secretary_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    secretary = db.relationship('User', foreign_keys=secretary_id)
    treasurer_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    treasurer = db.relationship('User', foreign_keys=treasurer_id)
    inventory_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    inventory = db.relationship('User', foreign_keys=inventory_id)

    def __repr__(self):
        return "<Club ({})>".format(self.name)


class Membership(db.Model):
    """A membership is a contract between user and club and has a defined start
    and end time."""
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', uselist=False, backref='memberships')
    club_id = db.Column(db.Integer, db.ForeignKey('club.id'), nullable=False)
    club = db.relationship('Club', uselist=False, backref='memberships')
    valid_from = db.Column(db.Date, nullable=False)
    valid_till = db.Column(db.Date, nullable=False)
    confirmed = db.Column(db.Boolean, default=False)

    def confirm(self, token):
        self.confirmed = True
        db.session.add(self)
        return True

    def __repr__(self):
        return "<Membership ({}, {}, from {} to {})>".format(
            self.user, self.club, self.valid_from, self.valid_till
        )


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    category = db.Column(db.String(16), nullable=False)
    email = db.Column(db.String(256))
    website = db.Column(db.String(256))
    address = db.Column(db.Text)
    officer_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    officer = db.relationship('User', foreign_keys=officer_id)
    deputy_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    deputy = db.relationship('User', foreign_keys=deputy_id)

    def __repr__(self):
        return "<Company ({})>".format(self.name)


class Employment(db.Model):
    """An employment connects a user and a company. It has no start and end,
    and only one employment is allowed at any instance of time."""
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', uselist=False, backref='employments')
    company_id = db.Column(
        db.Integer, db.ForeignKey('company.id'), nullable=False)
    company = db.relationship('Company', uselist=False, backref='employments')

    __table_args__ = (
        db.UniqueConstraint(
            'user_id', 'company_id', name='unique_user_company'),
    )

    def __repr__(self):
        return "<Employment ({}, {})>".format(
            self.user, self.company,
        )


class Partner(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), unique=True, nullable=False)
    user = db.relationship('User', foreign_keys=user_id)
    partner_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), nullable=False)
    partner = db.relationship('User', foreign_keys=partner_id)

    __table_args__ = (
        db.UniqueConstraint(
            'user_id', 'partner_id', name='unique_user_partner'),
    )

    def __repr__(self):
        return "<Partner ({}, {})>".format(self.user, self.partner)


class Child(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', foreign_keys=user_id)
    child_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), nullable=False)
    child = db.relationship('User', foreign_keys=child_id)

    def __repr__(self):
        return "<Child ({}, {})>".format(self.user, self.child)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    firstname = db.Column(db.String(64), index=True)
    lastname = db.Column(db.String(64), index=True)
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except Exception:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except Exception:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except Exception:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True

    def __repr__(self):
        return "<User ({} {})>".format(self.firstname, self.lastname)
