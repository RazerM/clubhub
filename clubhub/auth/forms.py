from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from wtforms import ValidationError
import safe

from clubhub.models import User


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')

    def validate_email(self, field):
        field.data = field.data.lower()


class RegistrationForm(FlaskForm):
    email = StringField(
        'Email', validators=[DataRequired(), Length(1, 64), Email()])
    firstname = StringField(
        'Firstname', validators=[DataRequired(), Length(1, 64)])
    lastname = StringField(
        'Lastname', validators=[DataRequired(), Length(1, 64)])
    password = PasswordField('Password', validators=[
        DataRequired(), EqualTo('password2', message='Passwords must match.')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_email(self, field):
        field.data = field.data.lower()
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

    def validate_password(self, field):
        strength = safe.check(field.data)
        if not strength.valid:
            raise ValidationError('Please choose a better password.')


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Old password', validators=[DataRequired()])
    password = PasswordField('New password', validators=[
        DataRequired(), EqualTo('password2', message='Passwords must match')])
    password2 = PasswordField(
        'Confirm new password', validators=[DataRequired()])
    submit = SubmitField('Update Password')

    def validate_password(self, field):
        strength = safe.check(field.data)
        if not strength.valid:
            raise ValidationError('Please choose a better password.')


class PasswordResetRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    submit = SubmitField('Reset Password')

    def validate_email(self, field):
        field.data = field.data.lower()


class PasswordResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    password = PasswordField('New Password', validators=[
        DataRequired(), EqualTo('password2', message='Passwords must match')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Reset Password')

    def validate_email(self, field):
        field.data = field.data.lower()
        if User.query.filter_by(email=field.data).first() is None:
            raise ValidationError('Unknown email address.')

    def validate_password(self, field):
        strength = safe.check(field.data)
        if not strength.valid:
            raise ValidationError('Please choose a better password.')


class ChangeEmailForm(FlaskForm):
    email = StringField('New Email', validators=[DataRequired(), Length(1, 64),
                                                 Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Update Email Address')

    def validate_email(self, field):
        field.data = field.data.lower()
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')
