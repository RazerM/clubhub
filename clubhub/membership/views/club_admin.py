from dateutil.parser import parse

from flask import render_template, flash, redirect, abort, request, url_for
from flask_login import current_user, login_required

from clubhub.core import db
from clubhub.models import User, Membership, Club
from clubhub.email import send_email
from clubhub.utils import require_club_admin, parse_arg
from clubhub.utils.membership import get_membership_requests,\
    get_conflicting_memberships, get_active_members_of_club
from .. import membership
from ..forms import ConfirmMembershipForm, ModifyMembershipForm,\
    CreateMembershipForm


@membership.route('/create-membership', methods=['GET', 'POST'])
@login_required
def create_membership():
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    require_club_admin(current_user, club)
    form = CreateMembershipForm()

    if form.validate_on_submit():
        user = User.query.filter_by(email=form.user_email.data).first()
        valid_from = form.valid_from.data
        valid_till = form.valid_till.data
        conflicting_memberships = get_conflicting_memberships(
            user.id, club.id, valid_from, valid_till)
        if not conflicting_memberships:
            membership = Membership(
                user=user, club=club,
                valid_from=valid_from, valid_till=valid_till,
                confirmed=True)
            db.session.add(membership)
            db.session.commit()
            flash('Membership for {} {} for the {} Club was created.'.format(
                user.firstname, user.lastname, club.name))
            send_email(
                user.email, 'Membership Created',
                'membership/email/confirm_membership',
                user=user, club=club,
                valid_from=valid_from, valid_till=valid_till)
            return redirect('/')
        else:
            flash('The requested membership is in conflict with the following '
                  'existing membership(s):')
            for conflicting_membership in conflicting_memberships:
                flash('Membership from {} till {}.'.format(
                    conflicting_membership.valid_from,
                    conflicting_membership.valid_till))
    else:
        form.club.data = club.name
    return render_template('form.html', title="Create Membership", form=form)


@membership.route('/confirm-membership-request', methods=['GET', 'POST'])
@login_required
def confirm_membership_request():
    user_id = parse_arg('user_id', int)
    user = User.query.get_or_404(user_id)
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    require_club_admin(current_user, club)

    valid_from = request.args.get('valid_from')
    valid_till = request.args.get('valid_till')
    if not valid_from or not valid_till:
        abort(400)
    try:
        valid_from = parse(valid_from).date()
        valid_till = parse(valid_till).date()
    except Exception:
        abort(400)

    membership = Membership.query.filter_by(
        user=user,
        club=club,
        valid_from=valid_from,
        valid_till=valid_till,
        confirmed=True).first()
    if membership:
        flash('This membership request was already approved.')
        return redirect(url_for('.list_membership_requests', club_id=club.id))

    membership = Membership.query.filter_by(
        user=user,
        club=club,
        valid_from=valid_from,
        valid_till=valid_till,
        confirmed=False).first()
    if not membership:
        abort(404)

    form = ConfirmMembershipForm(obj=membership)
    if form.validate_on_submit():
        membership.valid_from = form.valid_from.data
        membership.valid_till = form.valid_till.data
        conflicting_memberships = get_conflicting_memberships(
            user.id, club.id, valid_from, valid_till)
        if not conflicting_memberships:
            membership.confirmed = True
            db.session.add(membership)
            flash('Membership for {} {} for the {} Club was created.'.format(
                user.firstname, user.lastname, club.name))
            if send_email(
                user.email, 'Membership Confirmed',
                'membership/email/confirm_membership',
                user=user,
                club=club,
                valid_from=form.valid_from.data,
                    valid_till=form.valid_till.data):
                pass
            return redirect(
                url_for('.list_membership_requests', club_id=club.id))
        else:
            flash('The requested membership is in conflict with the following '
                  'existing membership(s):')
            for conflicting_membership in conflicting_memberships:
                flash('Membership from {} till {}.'.format(
                    conflicting_membership.valid_from,
                    conflicting_membership.valid_till))
    else:
        form.user.data = '{} {}'.format(user.firstname, user.lastname)
        form.club.data = '{}'.format(club.name)
    note = """Please confirm the membership request. You may adjust the valid
    time of the membership."""
    return render_template('form.html', title="Confirm Membership Request",
                           note=note, form=form)


@membership.route('/list-members')
@login_required
def list_members():
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    require_club_admin(current_user, club)
    members, memberships = get_active_members_of_club(club)
    members = sorted(members, key=lambda x: x.firstname.lower())
    members_email = ','.join([member.email for member in members])
    context = {
        'club': club,
        'members': members,
        'members_email': members_email
    }
    return render_template('membership/list_members.html', context=context)


@membership.route('/list-memberships')
@login_required
def list_memberships():
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    require_club_admin(current_user, club)
    memberships = Membership.query.filter_by(
        club=club, confirmed=True).order_by(
            Membership.valid_from.desc()).all()
    context = {
        'club': club,
        'memberships': memberships
    }
    return render_template('membership/list_memberships.html', context=context)


@membership.route('/list-membership-requests')
@login_required
def list_membership_requests():
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    require_club_admin(current_user, club)
    users, requests = get_membership_requests(club)
    Membership.query.filter_by(club=club, confirmed=False)
    users_requests = zip(users, requests) if requests else None
    context = {
        'club': club,
        'users_requests': users_requests
    }
    return render_template(
        'membership/list_membership_requests.html', context=context)


@membership.route('/modify-membership', methods=['GET', 'POST'])
@login_required
def modify_membership():
    membership_id = parse_arg('membership_id', int)
    membership = Membership.query.get_or_404(membership_id)
    user = User.query.get_or_404(membership.user_id)
    club = Club.query.get_or_404(membership.club_id)
    require_club_admin(current_user, club)

    form = ModifyMembershipForm()
    if form.validate_on_submit():
        valid_from = form.valid_from.data
        valid_till = form.valid_till.data
        conflicting_memberships = get_conflicting_memberships(
            user.id, club.id, valid_from, valid_till,
            exclude_membership_id=membership.id)
        if not conflicting_memberships:
            membership.valid_from = valid_from
            membership.valid_till = valid_till
            db.session.add(membership)
            db.session.commit()
            flash('Membership was modified.')
            return redirect(url_for('.list_memberships', club_id=club.id))
        else:
            flash('The requested membership is in conflict with the following '
                  'existing membership(s):')
            for conflicting_membership in conflicting_memberships:
                flash('Membership from {} till {}.'.format(
                    conflicting_membership.valid_from,
                    conflicting_membership.valid_till))
    else:
        form.user_email.data = user.email
        form.club.data = club.name
        form.valid_from.data = membership.valid_from
        form.valid_till.data = membership.valid_till
    return render_template('form.html', title="Modify Membership", form=form)


@membership.route('/delete-membership-request', methods=['GET', 'POST'])
@login_required
def delete_membership_request():
    membership_id = parse_arg('membership_request_id', int)
    membership = Membership.query.get_or_404(membership_id)
    if membership.confirmed:
        abort(400)
    club = Club.query.get_or_404(membership.club_id)
    require_club_admin(current_user, club)
    db.session.delete(membership)
    flash('Membership request deleted.'.format(club.name))
    return redirect(url_for('.list_membership_requests', club_id=club.id))
