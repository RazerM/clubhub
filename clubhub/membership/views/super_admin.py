import collections
from datetime import date

from flask import render_template
from flask_login import login_required

from clubhub.models import Club, Membership
from clubhub.utils import super_admin_required
from .. import membership
from ..forms import SelectYearForm, SelectDateForm


@membership.route('/list-members-by-year',  methods=['GET', 'POST'])
@login_required
@super_admin_required
def list_members_by_year():
    form = SelectYearForm()
    choices = [(0, "---")]
    choices.extend(
        [(c.id, c.name) for c in Club.query.order_by(Club.name).all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        year = form.year.data
        club = Club.query.get(form.club_id.data)
        club_members = collections.OrderedDict()
        if club:
            clubs = [club]
        else:
            clubs = Club.query.order_by(Club.name).all()
        for club in clubs:
            memberships = Membership.query.filter_by(club=club, confirmed=True)
            memberships = memberships.filter(
                Membership.valid_from <= date(year, 12, 31),
                Membership.valid_till >= date(year, 1, 1),
                ).all()
            members = []
            for membership in memberships:
                members.append(membership.user)
            club_members[club] = sorted(
                members, key=lambda x: x.firstname.lower())
        context = {
            'club_members': club_members
        }
        return render_template(
            'membership/list_members_by_club.html', context=context)

    return render_template('form.html', form=form)


@membership.route('/list-members-by-date',  methods=['GET', 'POST'])
@login_required
@super_admin_required
def list_members_by_date():
    form = SelectDateForm()
    choices = [(0, "---")]
    choices.extend(
        [(c.id, c.name) for c in Club.query.order_by(Club.name).all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        date = form.date.data
        club = Club.query.get(form.club_id.data)
        club_members = collections.OrderedDict()
        if club:
            clubs = [club]
        else:
            clubs = Club.query.order_by(Club.name).all()
        for club in clubs:
            memberships = Membership.query.filter_by(club=club, confirmed=True)
            memberships = memberships.filter(
                Membership.valid_from <= date,
                Membership.valid_till >= date,
                ).all()
            members = []
            for membership in memberships:
                members.append(membership.user)
            club_members[club] = sorted(
                members, key=lambda x: x.firstname.lower())
        context = {
            'club_members': club_members
        }
        return render_template(
            'membership/list_members_by_club.html', context=context)

    return render_template('form.html', form=form)
