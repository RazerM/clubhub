Dear {{ club.name }} Club management,

A membership request for your club has been issued
by {{ user.firstname}} {{ user.lastname}} ({{ user.email}}).

To process the request, please click on the following link:

{{ url_for('membership.confirm_membership_request', user_id=user.id, club_id=club.id, valid_from=valid_from, valid_till=valid_till, _external=True) }}

Note: replies to this email address are not monitored.
