import os
import hashlib

from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required, logout_user

from clubhub.core import db
from clubhub.email import send_email
from clubhub.utils.account import get_parent, get_other_half, \
    is_partner_or_child
from clubhub.utils.company import is_employee
from clubhub.models import User, Profile, Employment, Company, Partner, Child
from .. import account
from ..forms import UpdateAccountForm, DeactivateAccountRequestForm


@account.before_app_request
def before_request():
    if request.url_rule and\
            'membership.request_membership' in request.url_rule.endpoint and\
            current_user.is_authenticated and\
            current_user.confirmed and\
            not is_employee(current_user) and\
            not is_partner_or_child(current_user):
        flash('Please update your account with one of the three options.')
        return redirect(url_for('account.update_my_account'))


@account.route('/details-my-account')
@login_required
def details_my_account():
    user = User.query.get_or_404(current_user.id)
    gravatar = hashlib.md5(user.email.encode('utf-8')).hexdigest()
    profile = Profile.query.filter_by(user=current_user).first()
    employment = Employment.query.filter_by(user=current_user).first()
    company = Company.query.get(employment.company_id) if employment\
        else None
    partner = get_other_half(partner=user)
    parent = get_parent(user)

    context = {
        'user': user,
        'profile': profile,
        'company': company,
        'partner': partner,
        'parent': parent,
        'gravatar': gravatar,
    }
    return render_template(
        'account/details_account.html', title="My Account", context=context)


@account.route('/update-my-account', methods=['GET', 'POST'])
@login_required
def update_my_account():
    user = current_user
    employment = Employment.query.filter_by(user=user).first()
    company = Company.query.get(employment.company_id) if employment\
        else None

    # load if profile exists, otherwise create one
    profile = Profile.query.filter_by(user=user).first()
    if not profile:
        profile = Profile()
        profile.user = user
        db.session.add(profile)
        db.session.commit()

    form = UpdateAccountForm(obj=profile)
    choices = [(0, "---")]
    choices.extend(
        [(c.id, c.name) for c in Company.query.order_by(Company.name).all()])
    form.company_id.choices = choices
    if form.validate_on_submit():
        user.firstname = form.firstname.data
        user.lastname = form.lastname.data
        db.session.add(user)
        profile.phone = form.phone.data
        profile.about = form.about.data
        db.session.add(profile)

        if form.company_id.data:
            # User works for company, hence delete any existing other relation
            Child.query.filter_by(child=user).delete()
            Partner.query.filter_by(partner=user).delete()
            # check if this is a change of company or first definition
            if employment:
                employment.company_id = form.company_id.data
            else:
                employment = Employment(
                    user=user,
                    company_id=form.company_id.data
                )
            db.session.add(employment)
            db.session.commit()
        else:
            if employment:
                db.session.delete(employment)
                db.session.commit()

            if form.partner_email.data:
                # User is partner, delete as-child record if any
                Child.query.filter_by(child=user).delete()
                employee = User.query.filter_by(
                    email=form.partner_email.data).first()
                # check if record exists
                partner_of_user = Partner.query.filter_by(
                    user=employee, partner=user).first()
                # if record does not exist, create it
                if not partner_of_user:
                    partner_of_user = Partner(
                        user=employee,
                        partner=user,
                    )
                    db.session.add(partner_of_user)
                    db.session.commit()
                    send_email(
                        employee.email, 'Your Partner Registered',
                        'account/email/partner_registered',
                        employee=employee, partner=user
                    )
            elif form.parent_email.data:
                # User is child, delete as-partner record if any
                Partner.query.filter_by(partner=user).delete()
                employee = User.query.filter_by(
                    email=form.parent_email.data).first()
                # check if record exists
                child_of_user = Child.query.filter_by(
                    user=employee, child=user).first()
                # if record does not exist, create it
                if not child_of_user:
                    child_of_user = Child(user=employee, child=user)
                    db.session.add(child_of_user)
                    db.session.commit()
                    send_email(employee.email, 'Your Child Registered',
                               'account/email/child_registered',
                               employee=employee, child=user)

        flash('Account was updated.')
        return redirect(url_for('.details_my_account'))
    else:
        form.firstname.data = user.firstname
        form.lastname.data = user.lastname
        if company:
            form.company_id.data = company.id
        partner = Partner.query.filter_by(partner=user).first()
        if partner:
            form.partner_email.data = User.query.get(partner.user_id).email
        parent = Child.query.filter_by(child=user).first()
        if parent:
            form.parent_email.data = User.query.get(parent.user_id).email

    note = """If you work for ESA/EUMETSAT please select the company from the
    list. If you register as family member, only provide the email of your
    partner."""
    return render_template(
        'form.html', title="Update Account", note=note, form=form)


@account.route('/deactivate-my-account', methods=['GET', 'POST'])
@login_required
def deactivate_my_account():
    form = DeactivateAccountRequestForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            send_email(
                os.environ.get('FEEDBACK_EMAIL'),
                'Request to Deactivate Account',
                'account/email/deactivate_account_request',
                user=current_user)
            logout_user()
            flash(
                'Your request to disable your account has been sent by email.')
            return redirect('/')
    note = """Deactivation of your account will disable your login permanently.
    Only deactivate if you are sure you will not use your account anymore.
    An email will be sent to administration to confirm account deactivation."""
    return render_template(
        'form.html', title="Deactive Account", note=note, form=form)
