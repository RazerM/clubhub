import hashlib

from flask import render_template
from flask_login import current_user, login_required

from clubhub.utils import parse_arg, require_user_admin
from clubhub.utils.account import get_parent, get_other_half
from clubhub.models import User, Profile, Employment, Company
from .. import account


@account.route('/details-user-account')
@login_required
def details_user_account():
    user_id = parse_arg('user_id', int)
    user = User.query.get_or_404(user_id)
    require_user_admin(current_user, user)

    gravatar = hashlib.md5(user.email.encode('utf-8')).hexdigest()
    profile = Profile.query.filter_by(user=user).first()
    employment = Employment.query.filter_by(user=user).first()
    company = Company.query.get(employment.company_id) if employment\
        else None
    partner = get_other_half(partner=user)
    parent = get_parent(user)

    context = {
            'user': user,
            'profile': profile,
            'company': company,
            'partner': partner,
            'parent': parent,
            'gravatar': gravatar,
        }
    return render_template(
        'account/details_account.html', title="User Account", context=context)
