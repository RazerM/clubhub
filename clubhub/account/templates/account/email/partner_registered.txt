Hello {{ employee.firstname }} {{ employee.lastname }},

{{ partner.firstname }} {{ partner.lastname }} ({{ partner.email }}) has registered as your partner.


Note: replies to this email address are not monitored.
