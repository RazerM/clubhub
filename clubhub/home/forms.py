from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField
from wtforms.validators import Length


class FeedbackForm(FlaskForm):
    text = TextAreaField('', validators=[Length(max=4000)])
    submit = SubmitField()
