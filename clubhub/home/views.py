import os

from flask import render_template, redirect, url_for, flash
from flask_login import current_user, login_required

from clubhub.models import Club, Company
from clubhub.email import send_email
from clubhub.utils import parse_arg
from clubhub.utils.club import get_my_clubs, get_clubs_to_admin, get_club_roles
from clubhub.utils.membership import get_active_memberships
from clubhub.utils.company import get_company_roles, get_companies_to_admin
from . import home
from .forms import FeedbackForm


@home.route('/')
def index():
    if current_user.is_authenticated:
        my_clubs = get_my_clubs(current_user)
        my_clubs_to_admin = get_clubs_to_admin(current_user)
        my_companies_to_admin = get_companies_to_admin(current_user)
    else:
        my_clubs = None
        my_clubs_to_admin = None
        my_companies_to_admin = None
    context = {
        'my_clubs': my_clubs,
        'my_clubs_to_admin': my_clubs_to_admin,
        'my_companies_to_admin': my_companies_to_admin
    }
    return render_template('home/index.html', context=context)


@home.route('/about')
def about():
    return render_template('home/about.html')


@home.route('/user_guide')
def user_guide():
    return render_template('home/user_guide.html')


@home.route('/privacy_policy')
def privacy_policy():
    return render_template('home/privacy_policy.html')


@home.route('/feedback', methods=['GET', 'POST'])
def feedback():
    form = FeedbackForm()
    if form.validate_on_submit():
        user = current_user if current_user.is_authenticated else None
        text = form.text.data
        if send_email(os.environ.get('FEEDBACK_EMAIL'), 'User Feedback',
                      'home/email/feedback', user=user, text=text):
            flash('Thanks for your feedback!')
        return redirect(url_for('home.feedback'))
    return render_template('home/feedback.html', form=form)


@home.route('/menu-my-club')
@login_required
def menu_my_club():
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    memberships = get_active_memberships(current_user, club)
    context = {
        'club': club,
        'memberships': memberships,
    }
    return render_template('home/menu_my_club.html', context=context)


@home.route('/menu-club-administration')
@login_required
def menu_club_administration():
    club_id = parse_arg('club_id', int)
    club = Club.query.get_or_404(club_id)
    roles = get_club_roles(current_user, club)
    context = {
        'club': club,
        'roles': ", ".join(roles),
    }
    return render_template(
        'home/menu_club_administration.html', context=context)


@home.route('/menu-company-administration')
@login_required
def menu_company_administration():
    company_id = parse_arg('company_id', int)
    company = Company.query.get_or_404(company_id)
    roles = get_company_roles(current_user, company)
    context = {
        'company': company,
        'roles': ", ".join(roles),
    }
    return render_template(
        'home/menu_company_administration.html', context=context)
