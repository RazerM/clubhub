# ClubHub

A web application powered by [Flask](http://flask.pocoo.org) for club and
membership management.

## Installation

### Bare Installation

To install ClubHub bare-bone on your machine, follow the instructions below.
If you instead prefer to quickly run it as a docker container, see the section
below.

You will need to have MariaDB (or MySQL) installed.

Clone or download the repository to your computer.

In the root directory run the following commands to prepare the Python
dependencies (you may want to use a virtualenv for that).

```
$ pip install -r requirements.txt
```

Then copy the *.env.template* file to *.env* and make adjustments as needed.

And you're set! To start the server, run the following commands.

```
$ export FLASK_APP=clubhub
$ flask run -h 0.0.0.0
```

To verify that ClubHub is running open your browser and visit http://localhost:5000.

### Docker

As an alternative to the installation described above you can run ClubHub as
a docker container. To do so, you first need to have installed docker and
docker-compose on your machine.

Then clone or download the repository to your computer. In fact, you would only need the files *Dockerfile* and *docker-compose.yml*. Then run `docker-compose up`. Tapir will be available at http://localhost:5000.

### First-Time Setup

You will need to initialize the database and create the admin account by hand
after a fresh install (applies to both, bare install and docker).

If you use a docker container, then enter it by running:

```
docker-compose run clubhub bash
```

Initialize and populate the database:

```
$ flask db init
$ flask db migrate
$ flask db upgrade
```

Then create the admin user. This is done in the flask shell.

```python
$ flask shell
from clubhub.models import User, SuperAdmin
from clubhub.core import db
user = User(email='admin@local.host', password='a-good-password-please')
user.confirmed = True
db.session.add(user)
db.session.commit()
admin = SuperAdmin()
admin.user = user
db.session.add(admin)
db.session.commit()
exit()
$ exit
```
