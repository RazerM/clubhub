FROM python:3.4-slim

RUN apt-get update \
  && apt-get install -y git libmariadbclient-dev gcc \
  && rm -rf /var/lib/apt/lists/*

RUN useradd dummy

WORKDIR /app
RUN git clone https://gitlab.com/artur-scholz/clubhub.git /app
RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn pytest
RUN chown -R dummy:dummy /app

USER dummy
